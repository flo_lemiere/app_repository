## Configuration du Repository YUM 
### Création du blob store :

 ![](img/Image1.png)
 ![](img/Image2.png)
 ![](img/Image3.png)
### Création du repository hosted
 ![](img/Image4.png) 
 ![](img/Image5.png)
 ![](img/Image6.png)
 ![](img/Image7.png)

##	Upload des paquets sur le repository
### 	Récupération des paquets
Il faut ensuite récupérer les paquets dans l’archive fournie.
Voici la liste des paquets nécessaires :

```
containerd.io-1.4.12-3.1.el7.x86_64.rpm 
containerd.io-1.4.12-3.1.el8.x86_64.rpm 
docker-ce-20.10.12-3.el7.x86_64.rpm 
docker-ce-cli-20.10.12-3.el7.x86_64.rpm 
docker-ce-rootless-extras-20.10.12-3.el7.x86_64.rpm 
docker-ce-selinux-17.03.3.ce-1.el7.noarch.rpm 
docker-scan-plugin-0.9.0-3.el7.x86_64.rpm 
```

###	Upload sur le repository
L’upload peut être effectué via l’interface :
 
![](img/Image8.png)
![](img/Image9.png)
![](img/Image10.png)

Une fois tous les paquets uploadés :

![](img/Image11.png)

##Création du fichier de configuration du repository

###Récupération de l’url
Le fichier de configuration doit être complété avec l’url du repo qui peut être récupéré sur le nexus :

![](img/Image12.png)
![](img/Image13.png)
![](img/Image14.png)
 

###Création du fichier 
En utilisant le template suivant, il faut créer un fichier nommé par exemple « docker_nexus.repo », et remplacer ***URL*** par l’url recuperé précédemment :

```
[dockerrepo]
name=Docker Nexus Repository
baseurl=***URL***
enabled=1
priority=1
proxy=_none_

[docker-ce-stable]
name=Docker CE Stable - $basearch
baseurl=***URL***
enabled=1
sslverify=0
proxy=_none_

[docker-ce-stable-debuginfo]
name=Docker CE Stable - Debuginfo $basearch
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-stable-source]
name=Docker CE Stable - Sources
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test]
name=Docker CE Test - $basearch
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test-debuginfo]
name=Docker CE Test - Debuginfo $basearch
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test-source]
name=Docker CE Test - Sources
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly]
name=Docker CE Nightly - $basearch
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly-debuginfo]
name=Docker CE Nightly - Debuginfo $basearch
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly-source]
name=Docker CE Nightly - Sources
baseurl=***URL***
enabled=0
sslverify=0
proxy=_none_

```

( Il est a noté que l’on n’ active pas la vérification gpg )


###	Upload du fichier sur le dépôt raw DGFIP

Ce fichier doit être ensuite uploadé sur un repository disponible lors de l’instanciation, par exemple utili-ser l’app-repository : http://nexus-cloud.appli.dgfip/repository/app_repository/docker/docker_nexus.repo 

##Compléter les variables 
Il faut enfin renseigner les paramètres du fichier terraform.tfvars
```
docker_yum_gpg_key = " http://nexus-cloud.appli.dgfip/repository/app_repository/docker/docker-gpg"

docker_yum_repo_url = " http://nexus-cloud.appli.dgfip/repository/app_repository/docker/docker_nexus.repo"
```
La clé gpg est fournie dans l’app-repository, elle n’a pas besoin de correspondre au repository nexus car l’on ne fait pas de vérification, mais un download est effectué par le rôle et bloque l’instanciation s’il n’est pas effectué.

##Mis a jour du repository pour Rocky + Centos

On installe desormais les agents sur Centos7 et sur Rocky8, docker nécessite une version différente de l'un des paquets ( a savoir containerd ) 

Il faut donc modifier le fonctionnement du repository, en renseignant un sous repertoire pour chaques versions.

Le fichier docker_nexus.repo doit desormais contenir la variable $releasever dans ses urls, voici par exemple celui utilisé chez BAW:

```

[dockerrepo]
name=Docker Nexus Repository
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=1
priority=1
proxy=_none_


[docker-ce-stable]
name=Docker CE Stable - $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=1
sslverify=0
proxy=_none_

[docker-ce-stable-debuginfo]
name=Docker CE Stable - Debuginfo $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-stable-source]
name=Docker CE Stable - Sources
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test]
name=Docker CE Test - $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test-debuginfo]
name=Docker CE Test - Debuginfo $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-test-source]
name=Docker CE Test - Sources
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly]
name=Docker CE Nightly - $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly-debuginfo]
name=Docker CE Nightly - Debuginfo $basearch
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

[docker-ce-nightly-source]
name=Docker CE Nightly - Sources
baseurl=https://nexus.b-a-w.tech/repository/yum/$releasever/
enabled=0
sslverify=0
proxy=_none_

```

Il faut également que le repository dans Nexus contienne deux repertoires pour les deux versions:
- Un repertoire nommé "7"
- Un repertoire nommé "8"

Dans le repertoir nommé 7 il faudra uploader les paquets fournie dans l'archive Paquets_Docker_EL7.zip,  et dans le repertoire 8
il faudra uploader les paquets de l'archive Paquets_Docker_EL8.zip. 

Ci dessous le resultat escompté

![](img/Image15.png)